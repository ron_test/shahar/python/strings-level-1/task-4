def find_first_repeated_char(input_string):
    unique_chars = set()

    for char in input_string:
        if char in unique_chars:
            return char
        unique_chars.add(char)


input_string = input("enter a string: ")
print(find_first_repeated_char(input_string))
